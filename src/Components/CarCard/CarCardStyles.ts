import { TextStyle, ViewStyle } from "react-native";
import { widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

export default {
    modeloText: {
        color: '#FFFFFF',
        fontFamily: 'Poppins_300Light',
        fontSize: 25
    } as TextStyle,

    tituloText: {
        color: '#FFFFFF',
        fontFamily: 'Poppins_300Light',
        fontSize: 17
    } as TextStyle,

    buttonText: {
        color: '#FFFFFF',
        fontFamily: 'Poppins_300Light'
    } as TextStyle,

    circle: {
        marginTop:"35%",
        marginLeft:"20%",
        backgroundColor: '#FFFFFF',
        borderRadius: 100,
        width: wp("10%"),
        height: wp("10%"),
        alignItems: 'center',
        justifyContent: 'center'
    } as ViewStyle,

    card: {
        marginTop:"7%", 
        marginBottom:"8%"
    } as ViewStyle,

    hstack: {
        position: 'absolute', 
        flexDirection: 'row'
    } as ViewStyle,

    carImage: {
        marginTop:"30%", 
        marginLeft: "10%"
    } as ViewStyle,

    carImageSize: {
        width: wp("40%"), 
        height: hp("9%")
    }
}