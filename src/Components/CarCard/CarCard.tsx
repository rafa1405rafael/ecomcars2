import React from 'react'
import { MaterialCommunityIcons } from '@expo/vector-icons'
import { Image, View, Text, TouchableOpacity } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

import styles from './CarCardStyles'

export default function CarCard (props : any) {
    return (
        <View style={styles.card}>
            <TouchableOpacity onPress={props.onPress}>

                {/* Box do Carro */}
                <View style={{ backgroundColor: props.cor, borderRadius: 25, padding: 15, width: wp("85%"), height: hp("15%")}}>
                    <Text style={styles.modeloText}>{props.modelo}</Text>
                    <Text style={styles.tituloText}>{props.titulo}</Text>
                </View>

                {/* Hstack do carro + icon */}
                <View style={styles.hstack}>

                    {/* Imagem do carro */}
                    <View style={styles.carImage}>
                        <Image
                            style={styles.carImageSize}
                            source={{ uri: props.imagem }}
                            alt={'Imagem do Carro'}
                        />
                    </View>

                    {/* Icon */}
                    <View style={styles.circle}>
                        <MaterialCommunityIcons
                            name="arrow-right"
                            size={30}
                            color={'#A07A28'}
                        />
                    </View>

                </View>
            </TouchableOpacity>           
        </View>

    )
}